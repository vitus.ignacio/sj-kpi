const CONSTANTS = {
  ENV_PRODUCTION: 'production',
  ENV_DEVELOPMENT: 'development',
  ENV_TESTING: 'testing',
  WEBHOOK_TEST_PATH: 'https://hooks.slack.com/services/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',
  WEBHOOK_PROD_PATH: 'https://hooks.slack.com/services/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',
  SOCKETIO_UPDATE_KPI: 'update_kpi',
  SOCKETIO_UPDATE_GITLAB: 'update_gitlab',
  SOCKETIO_UPDATE_JIRA_INFOS: 'update_jira_infos',
  SOCKETIO_UPDATE_OPENWEATHERMAP: 'update_openweathermap'
}

module.exports = CONSTANTS
