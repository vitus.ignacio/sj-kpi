var _ = require('underscore'),
    axios = require('axios'),
    fs = require('fs'),
    moment = require('moment'),
    yaml = require('node-yaml'),
    schedule = require('node-schedule'),
    logger = require('simple-node-logger').createSimpleFileLogger('logs/jira-infos-events.log'),
    utils = require('./utils')

;(function() {

  logger.info('Read configuration file.')
  yaml.read('config.yml',
    {
      encoding: 'utf-8',
      schema: yaml.schema.defaultSafe,
    }, function(err, config) {
        
      const USERNAME = config.jira.username
      const PASSWORD = config.jira.password
      const DATA_URL_ALL_TASKS = 'https://jira.hyakuren.org/rest/api/2/search?jql=status%20!%3D%20Done&maxResults=1000&os_username=' + USERNAME + '&os_password=' + PASSWORD
      const DATA_URL_FRONTEND_TASKS = 'https://jira.hyakuren.org/rest/api/2/search?jql="Epic%20Link"%20in%20(MT-2%2C%20SJDEV-2)&maxResults=1000&os_username=' + USERNAME + '&os_password=' + PASSWORD
      const DATA_URL_SJDEV_TASKS = 'https://jira.hyakuren.org/rest/api/2/search?jql=project%20%3D%20SJDEV%20AND%20"Epic%20Link"%20in%20(MT-2%2C%20SJDEV-2)&maxResults=1000&os_username=' + USERNAME + '&os_password=' + PASSWORD
      const DATA_URL_MT_TASKS = 'https://jira.hyakuren.org/rest/api/2/search?jql=project%20%3D%20MT%20AND%20"Epic%20Link"%20in%20(MT-2%2C%20SJDEV-2)&maxResults=1000&os_username=' + USERNAME + '&os_password=' + PASSWORD
      const DATA_URL_DONE_TASKS = 'https://jira.hyakuren.org/rest/api/2/search?jql=status%20in%20(Done%2C%20Completed%2C%20"Ready%20for%20deploy"%2C%20"READY%20FOR%20TEST")%20AND%20"Epic%20Link"%20in%20(MT-2%2C%20SJDEV-2)&maxResults=1000&os_username=' + USERNAME + '&os_password=' + PASSWORD
      const DATA_URL_DOING_TASKS = 'https://jira.hyakuren.org/rest/api/2/search?jql=status%20in%20("In%20Progress"%2C%20"To%20Do"%2C%20"Develop%20Review"%2C%20"Platform%20Review"%2C%20Backlog%2C%20"Selected%20for%20Development")%20AND%20"Epic%20Link"%20in%20(MT-2%2C%20SJDEV-2)&maxResults=1000&os_username=' + USERNAME + '&os_password=' + PASSWORD
      const DATA_URL_QAQC_TASKS = 'https://jira.hyakuren.org/rest/api/2/search?jql=status%20in%20("IN%20TESTING"%2C%20"QA%20REVIEW"%2C%20"QC%20PASSED")%20AND%20"Epic%20Link"%20in%20(SJDEV-2%2C%20MT-2)&maxResults=1000&os_username=' + USERNAME + '&os_password=' + PASSWORD
      var current_release_sprint = config.jira.release_version

      var original_file_path = `${config.paths.default_orig}/${config.output_files[1]}`
      var destination_file_path = `${config.paths.default_dest}/${config.output_files[1]}`

      var data = {
        current_sprint: current_release_sprint,
        current_release: current_release_sprint,
        all_tasks: 0,
        frontend_tasks: 0,
        sjdev_tasks: 0,
        mt_tasks: 0,
        done_tasks: 0,
        doing_tasks: 0,
        qcqa_tasks: 0
      }, data_queue = [],
        scheduled_run = process.env.SCHEDULED_RUN || false

      var job = function() {
        logger.info(`Get general JIRA Frontend informations.`)
        data_queue.push(axios.get(DATA_URL_ALL_TASKS)
          .then(function(response) {
            logger.info(`Get all tasks count.`)
            if (response) {
              if (response.data) {
                data.all_tasks = response.data.total
              }
            }
          })
        )
        data_queue.push(axios.get(DATA_URL_FRONTEND_TASKS)
          .then(function(response) {
            logger.info(`Get all frontend tasks count.`)
            if (response) {
              if (response.data) {
                data.frontend_tasks = response.data.total
              }
            }
          })
        )
        data_queue.push(axios.get(DATA_URL_SJDEV_TASKS)
          .then(function(response) {
            logger.info(`Get all SJDEV tasks count.`)
            if (response) {
              if (response.data) {
                data.sjdev_tasks = response.data.total
              }
            }
          })
        )
        data_queue.push(axios.get(DATA_URL_MT_TASKS)
          .then(function(response) {
            logger.info(`Get all MT tasks count.`)
            if (response) {
              if (response.data) {
                data.mt_tasks = response.data.total
              }
            }
          })
        )
        data_queue.push(axios.get(DATA_URL_DONE_TASKS)
          .then(function(response) {
            logger.info(`Get all DONE tasks count.`)
            if (response) {
              if (response.data) {
                data.done_tasks = response.data.total
              }
            }
          })
        )
        data_queue.push(axios.get(DATA_URL_DOING_TASKS)
          .then(function(response) {
            logger.info(`Get all DOING tasks count.`)
            if (response) {
              if (response.data) {
                data.doing_tasks = response.data.total
              }
            }
          })
        )
        data_queue.push(axios.get(DATA_URL_QAQC_TASKS)
          .then(function(response) {
            logger.info(`Get all tasks being QCQA.`)
            if (response) {
              if (response.data) {
                data.qcqa_tasks = response.data.total
              }
            }
          })
        )
        axios.all(data_queue)
          .then(function() {
            logger.info(`Calculating additional informations.`)
            var t_done_tasks = data.done_tasks,
                t_doing_tasks = data.doing_tasks,
                t_qcqa_tasks = data.qcqa_tasks

            data.done_tasks = Math.round((t_done_tasks/data.frontend_tasks) * 100)
            data.doing_tasks = Math.round((t_doing_tasks/data.frontend_tasks) * 100)
            data.qcqa_tasks = Math.round((t_qcqa_tasks/data.frontend_tasks) * 100)

            logger.info(`Write to file ${original_file_path}.`)
            fs.writeFileSync(original_file_path,JSON.stringify(data))

            logger.info(`Copy data file to ${destination_file_path}.`)
            if (fs.existsSync(original_file_path))
            {
              fs.copyFileSync(original_file_path, destination_file_path)
              utils.log_changed_file(config, 1)
            }

            logger.info(`Process completed.`)
          })
      }

      logger.info(`Service started.`)
      if (scheduled_run == true) {
        logger.info('Scheduled job activate.')
        schedule.scheduleJob('*/10 * * * 1,2,3,4,5', job)
      }
      else {
        logger.info('Single-run job activate.')
        job()
      }

    })

})()

var done_yet = false
process.on('beforeExit', function() {
  if (!done_yet)
  {
    logger.info(`Service stopped.`)
    done_yet = true
  }
})
