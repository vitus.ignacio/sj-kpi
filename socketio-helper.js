/* This file should be scheduled to be run around 1 min each */

var _ = require('underscore'),
    fs = require('fs'),
    yaml = require('node-yaml'),
    moment = require('moment'),
    logger = require('simple-node-logger').createSimpleFileLogger('logs/socketio-helper-events.log'),
    CONSTANTS = require('./constants')

var event = process.env.EVENT
var public_socket = null

var close_socket_connection = function(response) {
  logger.info('Close socket connection.')
  public_socket.close()
}

var handle_request = function(event, socket, logger) {
  logger.info('Check event started.')
  if (event != null && typeof event !== 'undefined') {
    logger.info('Process SOCKETIO request.')
    switch (event) {
      case CONSTANTS.SOCKETIO_UPDATE_KPI:
        logger.info('Emit message to KPI notification services.')
        socket.emit('message', { source: 'kpi', fetched: true, updated_on: moment().unix() }, close_socket_connection)
        break;
      case CONSTANTS.SOCKETIO_UPDATE_GITLAB:
        logger.info(`Broadcast gitlab event to socket IO server.`)
        socket.emit('message', { source: 'gitlab', fetched: true, updated_on: moment().unix() }, close_socket_connection)
        break;
      case CONSTANTS.SOCKETIO_UPDATE_JIRA_INFOS:
        logger.info(`Broadcast jira-infos event to socket IO server.`)
        socket.emit('message', { source: 'jira-infos', fetched: true, updated_on: moment().unix() }, close_socket_connection)
        break;
      case CONSTANTS.SOCKETIO_UPDATE_OPENWEATHERMAP:
        logger.info(`Broadcast openweathermap event to socket IO server.`)
        socket.emit('message', { source: 'openweathermap', fetched: true, updated_on: moment().unix() }, close_socket_connection)
        break;
      default:
        logger.info('Being idle due to being given wrong/insufficient parameters.')
        break;
    }
    logger.info('SOCKETIO request completed.')
  } else {
    logger.info(`No events detected. Skip.`)
  }
}

logger.info('Read configuration file.')
yaml.read('config.yml',
  {
    encoding: 'utf-8',
    schema: yaml.schema.defaultSafe,
  }, function(err, config) {
    logger.info(`Service started.`)

    try {
      socket = require('socket.io-client')(`http://${config.socketio.server_ip}:8080/services`)
      socket.on('connect', function() { })
      socket.io.on('connect_error', function() {
        logger.info(`Unable to connect to ${config.socketio.server_ip}`)
        socket.close()
      })
      socket.on('disconnect', function() { })

      public_socket = socket

      logger.info('Check command-line parameters.')
      if (event == null || typeof event === 'undefined') {
        logger.info('No command-line parameters detected.')
        if (fs.existsSync(config.socketio.changed_files)) {
          var changes = fs.readFileSync(config.socketio.changed_files, 'utf8').split(require('os').EOL)
          _.each(_.uniq(_.without(changes, '')), function (change, key, list) {
            logger.info(`Execute SOCKETIO notification for ${change}.`)
            switch (change) {
              case config.output_files[0]:
                event = CONSTANTS.SOCKETIO_UPDATE_GITLAB
                break
              case config.output_files[1]:
                event = CONSTANTS.SOCKETIO_UPDATE_JIRA_INFOS
                break
              case config.output_files[2]:
                event = CONSTANTS.SOCKETIO_UPDATE_KPI
                break
              case config.output_files[3]:
                event = CONSTANTS.SOCKETIO_UPDATE_KPI
                break
              case config.output_files[4]:
                event = CONSTANTS.SOCKETIO_UPDATE_OPENWEATHERMAP
                break
              default:
                event = null
                break
            }

            logger.info(`Handle SOCKETIO notification for event ${event}.`)
            handle_request(event, socket, logger)
          })
        } else {
          logger.info('Changed file does not exist. Skip.')
          socket.close()
        }
      } else {
        logger.info('Command-line parameters detected.')
        handle_request(event, socket, logger)
      }

      logger.info(`Delete changed file metadata if any.`)
      if (fs.existsSync(config.socketio.changed_files)) {
        fs.unlinkSync(config.socketio.changed_files)
      }  else {
        logger.info('Changed file does not exist. Skip.')
      }

    } catch (exception) {
      logger.info(`Exception occurred: ${exception}`)
      socket.close()
    }
    
  }
)

var done_yet = false
process.on('beforeExit', function() {
  if (!done_yet) {
    logger.info(`Service stopped.`)
    done_yet = true
  }
})