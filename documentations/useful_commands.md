# Useful Commands
#### Run node command with environment variable in Windows Powershell
```
set-item Env:ENV "production"; set-item Env:SLACK_WEBHOOK 1; node gitlab.js
```
```
set-item Env:ENV "production"; set-item Env:SLACK_WEBHOOK 1; set-item Env:SCHEDULED_RUN 0; node gitlab.js
```
#### Run node command with environment variable in Bash shell
```
ENV=testing SLACK_WEBHOOK=0 SCHEDULED_RUN=0 node gitlab.js
```
```
EVENT=update_kpi node socketio-helper.js
```
