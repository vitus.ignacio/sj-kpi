var _ = require('underscore'),
  axios = require('axios'),
  fs = require('fs'),
  moment = require('moment'),
  yaml = require('node-yaml'),
  logger = require('simple-node-logger').createSimpleFileLogger('logs/gitlab-events.log'),
  schedule = require('node-schedule'),
  CONSTANTS = require('./constants'),
  utils = require('./utils')

var original_file_path, destination_file_path

var send_slack_webhook = process.env.SLACK_WEBHOOK || false,
    scheduled_run = process.env.SCHEDULED_RUN || false

var queue = []

var output = {
  open_merge_requests_with_frontend_label: [],
  release_merge_requests_with_frontend_label: [],
  updated_on: null
}

function Work() {
  this.opened_merge_requests_with_frontend_labels = []
  this.merged_merge_requests_with_frontend_labels = []
  this.release_merge_requests_with_frontend_labels = []
}

var generateRequestIids = function(current_count, step) {
  template = `&iids[]={0}`
  params = ''
  for (var i = current_count; i < current_count + step; i++) {
    params += template.replace('{0}', i.toString())
  }
  return params
}

var job = function(config) {
  var work = new Work()

  var gitlab_tomareru_app_project_id = config.jira.project_id,
      gitlab_tomareru_app_release_version = config.jira.release_version,
      gitlab_personal_access_token = config.gitlab.personal_access_token

  logger.info('Retrieve GITLAB data from service.')
  var current_count = parseInt(config.gitlab.min_iid)
  var step = parseInt(config.gitlab.step)

  axios.get('https://gitlab.hyakuren.org/api/v4/projects/9/merge_requests?private_token=' + gitlab_personal_access_token)
    .then(function(base_response) {
      base_max_iid = base_response.data[0].iid
      logger.info(`Current maximum base IID: ${base_max_iid}`)

      logger.info(`Process GITLAB data.`)
      while (current_count <= base_max_iid) {
        var url = 'https://gitlab.hyakuren.org/api/v4/projects/9/merge_requests?private_token=' + gitlab_personal_access_token + generateRequestIids(current_count, step)
        queue.push(axios.get(url)
          .then(function (response) {

            var raw = response.data
            _.each(raw, function (merge_request, key, object) {
              // logger.info(`Process IID ${merge_request.iid} in progress.`)

              if (merge_request.labels.indexOf('Frontend') != -1 && 
                  (merge_request.state == 'opened'))
              {
                work.opened_merge_requests_with_frontend_labels.push(
                  { 
                    title: merge_request.title,
                    author: merge_request.author.name,
                    author_avatar: merge_request.author.avatar_url,
                    assignee: merge_request.assignee.name,
                    assignee_identifier: merge_request.assignee.username,
                    assignee_avatar: merge_request.assignee.avatar_url,
                    work_in_progress: merge_request.work_in_progress,
                    milestone: merge_request.milestone != null && typeof merge_request.milestone !== 'undefined' ? merge_request.milestone.title : '',
                    web_url: merge_request.web_url,
                    labels: merge_request.labels
                  }
                )
              }
            })
            
          })
          .catch(function (err) {
            logger.info(`Error occurred. ${err}`)
          })
        )
        current_count += step
      }

      axios.all(queue)
        .then(axios.spread(function () {

          output.open_merge_requests_with_frontend_label = work.opened_merge_requests_with_frontend_labels
          output.updated_on = moment().unix()

          if (send_slack_webhook == true) {
            logger.info('Slack notification was removed.')
          } else {
            logger.info(`Write to file ${original_file_path}.`)
            fs.writeFileSync(original_file_path, JSON.stringify(output))

            logger.info(`Copy data file to ${destination_file_path}.`)
            if (fs.existsSync(original_file_path))
            {
              fs.copyFileSync(original_file_path, destination_file_path)
              utils.log_changed_file(config, 0)
            }
          }

        }))

    })
}

logger.info('Service started.')
;(function() {

  logger.info('Read configuration file.')
  yaml.read('config.yml',
    {
      encoding: 'utf-8',
      schema: yaml.schema.defaultSafe,
    }, function(err, config) {

      if (err) {
        logger.info('Configuration file not found.')
        return
      }
      if (config) {

        original_file_path = `${config.paths.default_orig}/${config.output_files[0]}`
        destination_file_path = `${config.paths.default_dest}/${config.output_files[0]}`

        if (scheduled_run == true) {
          logger.info('Scheduled job activate.')
          schedule.scheduleJob('*/15 * * * 1,2,3,4,5', function() {
            job(config)
          })
        }
        else {
          logger.info('Single-run job activate.')
          job(config)
        }

      }
    })
})()

var done_yet = false
process.on('beforeExit', function() {
  if (!done_yet)
  {
    logger.info(`Service stopped.`)
    done_yet = true
  }
})
