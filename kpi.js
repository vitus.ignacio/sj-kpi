var _ = require('underscore'),
    axios = require('axios'),
    fs = require('fs'),
    moment = require('moment'),
    yaml = require('node-yaml'),
    IncomingWebhook = require('@slack/client').IncomingWebhook,
    logger = require('simple-node-logger').createSimpleFileLogger('logs/kpi-events.log'),
    CONSTANTS = require('./constants'),
    utils = require('./utils')

var webhook_test_path = CONSTANTS.WEBHOOK_TEST_PATH,
    webhook_prod_path = CONSTANTS.WEBHOOK_PROD_PATH

var productivity_records = [],
    ranking_records = [],
    send_slack_webhook = process.env.SLACK_WEBHOOK || false

var webhook = new IncomingWebhook(process.env.ENV == CONSTANTS.ENV_PRODUCTION ? webhook_prod_path : webhook_test_path)

//
Math.better_round = function(number, precision) {
  var shift = function (number, precision, reverseShift) {
    if (reverseShift) {
      precision = -precision;
    }  
    numArray = number.toString().split('e');
    return +(numArray[0] + 'e' + (numArray[1] ? (+numArray[1] + precision) : precision));
  };
  return shift(Math.round(shift(number, precision, false)), precision, true);
}
function Subject(name, id)
{
  var _this = this

  this.id = id
  this.name = name
  this.all_tickets = []
  this.progress = {}
  this.total_tickets_count = 0

  this.calculate = function() {
    _this.total_tickets_count = _this.all_tickets.length
  }
}
function Progress(start_date, end_date, member)
{
  var _this = this

  this.time_spent = []
  this.total_time_spent = 0
  this.total_deltas = 0
  this.total_working_time = 0
  this.time_estimate = []
  this.total_time_estimate = 0
  this.estimate_vs_working_time_ratio = 0
  this.spent_vs_working_time_ratio = 0
  this.logability = 0
  this.productivity = 0
  this.today_logged_tickets = []
  this.yesterday_logged_tickets = []
  this.holidays = []

  // console.log('Start Date: ' + moment(start_date).format('DD/MM/YYYY hh:mm'))
  // console.log('End Date: ' + moment(end_date).format('DD/MM/YYYY hh:mm'))

  this.calculate = function() {
    _.each(_this.time_spent, function(value, key, object) {
      _this.total_time_spent += value
    })
    _.each(_this.time_estimate, function(value, key, object) {
      _this.total_time_estimate += value
    })
    if (start_date && end_date)
    {
      start = moment(start_date)
      end = moment(end_date)
      days = moment(end_date).diff(moment(start_date), 'days', true)
      while (end.diff(start, 'days') >= 0) {
        if (start.isoWeekday() === 6 || start.isoWeekday() === 7) {
          days--
        } else {
          formatted_start = start.format('DD/MM/YYYY')
          if (_this.holidays.indexOf(formatted_start) != -1) {
            days--
          } else {
            if (member != null && typeof member !== 'undefined') {
              if (member.holidays.length > 0) {
                if (member.holidays.indexOf(formatted_start) != -1) {
                  logger.info(`Found a holiday, process.`)
                  days--
                }
              }
            }
          }
        }
        start.add(1, 'days')
      }
      _this.total_working_time = Math.better_round(days * 8, 2)
    }
    else
    {
      throw Error('start_date and end_date must be set')
    }
    if (_this.total_working_time > 0)
    {
      _this.estimate_vs_working_time_ratio = Math.better_round(_this.total_time_estimate / _this.total_working_time, 2)
      _this.spent_vs_working_time_ratio = Math.better_round(_this.total_time_spent / _this.total_working_time, 2)
    }
    if (_this.estimate_vs_working_time_ratio > 0)
    {
      // _this.logability = Math.better_round(_this.spent_vs_working_time_ratio / _this.estimate_vs_working_time_ratio, 4)
      _this.logability = Math.better_round(_this.total_time_spent / _this.total_working_time, 4)
    }
    if (_this.total_time_spent > 0)
    {
      _this.productivity = Math.better_round((_this.total_time_estimate / _this.total_time_spent) * _this.logability, 2)
    }
  }
}
//

;(function() {

  logger.info('Read configuration file.')
  yaml.read('config.yml',
    {
      encoding: 'utf-8',
      schema: yaml.schema.defaultSafe,
    }, function(err, config) {

      var original_productivity_file_path = `${config.paths.default_orig}/${config.output_files[2]}`,
          destination_productivity_file_path = `${config.paths.default_dest}/${config.output_files[2]}`
          original_ranking_file_path = `${config.paths.default_orig}/${config.output_files[3]}`,
          destination_ranking_file_path = `${config.paths.default_dest}/${config.output_files[3]}`

      var username = config.jira.username
      var password = config.jira.password

      logger.info(`Service started.`)
      fs.readFile('data/members.json', 'utf8', function (err, member_data) {
        if (err) return
        
        var member_data = JSON.parse(member_data)
        var members = member_data.details
        if (members)
        {
          logger.info(`Check each member and perform computation.`)

          var holidays = member_data.holidays

          _.each(members, function(member, key, object) {
            logger.info(`Processing for member ${member.name} started.`)

            // logger.info(`Check issues for ${member.file_id}.`)
            axios.get('https://jira.hyakuren.org/rest/api/2/search?jql=key%20in%20workedIssues("2018/01/01",%20"2018/12/31",%20"' + member.file_id + '")%20ORDER%20BY%20createdDate%20ASC&maxResults=1000&os_username=' + username + '&os_password=' + password)
              .then(function (response) {
                logger.info(`Building member ${member.name} information started.`)
                var raw = response.data
                var ranking_subject = {
                  name: member.name
                }
                if (raw.total > 0)
                {
                  // Preprocessing
                  var subject = new Subject(member.name, member.file_id)
                  subject.progress = new Progress(
                    member.start_date ? moment(member.start_date, 'DD/MM/YYYY', true) : moment(raw.issues[0].fields.created), 
                    moment(), 
                    member
                  )
                  subject.progress.holidays = holidays
                  var processing_queue = []

                  _.each(raw.issues, function (issue, key, object) {
                    subject.all_tickets.push(issue.key)

                    subject.progress.time_estimate.push(Math.better_round(issue.fields.timeoriginalestimate / 3600, 2))

                    // logger.info(`Check worklogs for ${member.file_id}.`)
                    var promise = axios.get('https://jira.hyakuren.org/rest/api/2/issue/' + issue.key + '/worklog?os_username=' + username + '&os_password=' + password)
                        .then(function (response) {
                          var raw = response.data
                          if (raw.total > 0)
                          {
                            _.each(raw.worklogs, function (worklog, key, object) {
                              if (worklog.author.name.toLowerCase() == member.file_id)
                              {
                                if (moment(worklog.updated).format('DD/MM/YYYY') == moment().format('DD/MM/YYYY'))
                                {
                                  subject.progress.today_logged_tickets.push(issue.key + ':' + issue.fields.summary)
                                }
                                if (moment(worklog.updated).format('DD/MM/YYYY') == moment().add(-1, 'days').format('DD/MM/YYYY'))
                                {
                                  subject.progress.yesterday_logged_tickets.push(issue.key + ':' + issue.fields.summary)
                                }
                                subject.progress.time_spent.push(Math.better_round(worklog.timeSpentSeconds / 3600, 2))
                              }
                              // TODO: Debugging purpose
                              // else
                              // {
                                // console.log('AUTHOR: ' + worklog.author.key)
                                // console.log('MEMBER: ' + member.file_id)
                              // }
                            })
                          }
                        })
                        .catch(function (error) {
                          logger.info(`Error occurred with ${error}.`)
                        })

                    processing_queue.push(promise)
                  })

                  // Process subject data
                  subject.calculate()
                  
                  // Process tickets queue
                  axios.all(processing_queue).then(function () {
                    logger.info(`Finalizing member ${member.name} information started.`)
                    // console.log('Processing for `' + member.name + '` has been completed. Now do progress calculation')
                    subject.progress.calculate()
                    // console.log(subject)

                    // Send message to Slack
                    var remarks = ''
                    var remarks_list = []
                    var color = '#36a64f'
                    var title = 'Individual Performance Report'
                    if (subject.progress.logability <= 0.2)
                    {
                      color = '#ce0006'
                      remarks_list.push(' * You might have forgotten to log your time. Please check your JIRA tickets and add logs.\n')
                    }
                    if (subject.progress.logability > 1)
                    {
                      color = '#ffe100'
                      remarks_list.push(' * Please double check your tickets estimates and logged times. There might be inconsistencies.')
                    }
                    if (subject.progress.productivity <= 0.5)
                    {
                      color = '#ce0006'
                      remarks_list.push(' * Your performance seems low, please ensure that your tickets have proper estimation and logging.\n')
                    }
                    if (color != '#36a64f')
                    {
                      title = 'Reminder'
                      remarks += '*Remarks:*\n'
                      _.each(remarks_list, function (value, key, object) {
                        remarks += value
                      })
                    }

                    var logability_percentage = Math.better_round(subject.progress.logability * 100, 2)
                    var productivity_info = '<@' + member.slack_id + '>\n*Logability:* ' + (logability_percentage).toString() + '%'
                    var logged_tickets = ''
                    if (subject.progress.today_logged_tickets.length > 0)
                    {
                      logged_tickets += '*Today\'s Logged Tickets:*\n'
                      _.each(_.uniq(subject.progress.today_logged_tickets), function (ticket, key, object) {
                        logged_tickets += ticket + '\n'
                      })
                    }
                    var yesterday_tickets = ''
                    if (subject.progress.yesterday_logged_tickets.length > 0)
                    {
                      yesterday_tickets = '*Yesterday\'s Logged Tickets:*\n'
                      _.each(_.uniq(subject.progress.yesterday_logged_tickets), function (ticket, key, object) {
                        yesterday_tickets += ticket + '\n'
                      })
                    }
                    var productivity_rating = '*Productivity Rating:* '
                    if (subject.progress.productivity < 0.3) {
                      productivity_rating += 'F'
                    } else if (subject.progress.productivity >= 0.3 && subject.progress.productivity <= 0.5) {
                      productivity_rating += 'D'
                    } else if (subject.progress.productivity > 0.5 && subject.progress.productivity <= 0.6) {
                      productivity_rating += 'D+'
                    } else if (subject.progress.productivity > 0.6 && subject.progress.productivity <= 0.7) {
                      productivity_rating += 'C'
                    } else if (subject.progress.productivity > 0.7 && subject.progress.productivity <= 0.75) {
                      productivity_rating += 'C+'
                    } else if (subject.progress.productivity > 0.76 && subject.progress.productivity <= 0.83) {
                      productivity_rating += 'B'
                    } else if (subject.progress.productivity > 0.83 && subject.progress.productivity <= 0.89) {
                      productivity_rating += 'B+'
                    } else if (subject.progress.productivity > 0.89 && subject.progress.productivity <= 1) {
                      productivity_rating += 'A'
                    } else {
                      productivity_rating += 'A+'
                    }
                    ranking_subject.rank = subject.progress.productivity

                    if (send_slack_webhook == true)
                    {
                      webhook.send({
                        attachments: [
                            {
                                fallback: 'Required plain-text summary of the attachment.',
                                color: color,
                                title: title,
                                text: productivity_info + '\n' + productivity_rating + '\n' + (logged_tickets.length > 0 ? logged_tickets : '') + '\n' + (yesterday_tickets.length > 0 ? yesterday_tickets : '') ,
                                footer: 'Hyakurensenma FE'
                            }
                        ]
                      }, function(err, res) {
                        if (err) {
                          // console.log('Error:', err);
                        } else {
                          // console.log('Message sent: ', res);
                        }
                      });
                    } else {
                      // Write file
                      if (fs.existsSync(original_productivity_file_path))
                      {
                        fs.readFile(original_productivity_file_path, (err, data) => {
                          if (err) return;
                          if (data) {
                            var member_productivity_data = _.find(JSON.parse(data), function (d) {
                              return d.friendly_id == subject.id
                            })
                            member_productivity_data.productivity_data.push(logability_percentage)
                            member_productivity_data.last_updated = moment().unix()
                            productivity_records.push(member_productivity_data)
                          }
                        });
                        ranking_records.push(ranking_subject)
                      }
                    }
                    logger.info(`Processing for member ${member.name} done.`)
                  })
                }
              })
              .catch(function (error) {
                logger.info(`Error occurred with ${error}.`)
              })
          })
        }
      })

      var done_yet = false
      process.on('beforeExit', function () {
        if (!done_yet) {
          logger.info(`Post-processing started.`)
          // Output productivity records
          if (send_slack_webhook == false)
          {
            logger.info(`Write to file ${original_productivity_file_path} and ${original_ranking_file_path}.`)
            fs.writeFileSync(original_productivity_file_path, JSON.stringify(productivity_records))
            fs.writeFileSync(original_ranking_file_path, JSON.stringify(ranking_records))
            logger.info(`Copy files to destinations.`)
            if (fs.existsSync(original_productivity_file_path)) {
              fs.copyFileSync(original_productivity_file_path, destination_productivity_file_path)
              utils.log_changed_file(config, 2)
            }
            if (fs.existsSync(original_ranking_file_path)) {
              fs.copyFileSync(original_ranking_file_path, destination_ranking_file_path)
              utils.log_changed_file(config, 3)
            }
          }
          logger.info(`Post-processing done.`)
          logger.info(`Service stopped.`)
          done_yet = true
        }
      });

    })

})()
