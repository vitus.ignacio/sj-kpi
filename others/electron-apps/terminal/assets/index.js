var DEFAULT_SERVER_URL = 'http://127.0.0.1:8080',
    DEFAULT_CHANNEL_NAME = 'terminal-command',
    socket = null

var terminal = null
var credential_callback = null

$(() => {

  $.get('assets/config.json?_=' + $.uuid(), function(res) {
    if (res) {
      var config = JSON.parse(res)
      var server_configuration = []
      server_configuration.push(
        config.server_url !== null && typeof config.server_url !== 'undefined' ? config.server_url : DEFAULT_SERVER_URL
      )
      server_configuration.push(
        config.channel_name !== null && typeof config.channel_name !== 'undefined' ? config.channel_name : DEFAULT_CHANNEL_NAME
      )

      console.log(server_configuration.join('/'))
      socket = io(server_configuration.join('/'))

      terminal = $('.js-terminal').terminal(function(command) {
        if (command !== '') {
          if (command.trim().indexOf('goto') == 0) {
            window.location.href = '/' + command.substring(4, command.length).trim()
          } else {
            socket.emit('command', {
              type: 'execution',
              package: {
                command: command
              }
            })
          }
        }
      }, {
        greetings: 'Welcome to Steve\'s Terminal',
        name: 'steve_terminal',
        prompt: 'steve@terminal> ',
        login: function(user, password, callback) {
          credential_callback = callback
          socket.emit('command', {
            type: 'authentication',
            package: {
              user: user,
              password: password
            }
          })
        }
      })

      socket.on('connect', () => {})
      socket.on('response', (data) => {
        if (terminal != null) {
          switch (data.from) {
            case 'authentication':
              if (data.result.allow == true)
                credential_callback(data.result.auth_token)
              else
                credential_callback(null)
              break;
            case 'execution':
              if (data.result.status == true)
                  terminal.echo(data.result.output)
              break;
            default:
              break;
          }
        }
      })
      socket.on('disconnect', () => {})

    }
  })

})
