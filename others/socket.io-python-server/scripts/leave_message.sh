#!/bin/bash

NOW=$(date +"%d/%m/%Y %H:%M")

if [ -z "${1}" ]; then
  echo "message text is required"
else
  uuid=$(uuidgen)
  if [ -z "${2}" ]; then
    echo "free~${uuid}~${1}" >> bulletin_board.txt ;
  else
    if [ "${2}" = "pinned" ]; then
      echo "pinned~${uuid}~${1}" >> bulletin_board.txt ;
    fi
  fi
  echo "[${NOW}] \"${1}\" was delivered to bulletin board"
  wget -qbc http://0.0.0.0:8080/broadcast --post-data "type=leave_message" >/dev/null 2>/dev/null
fi
