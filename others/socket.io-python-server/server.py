from aiohttp import web
import socketio
import logging
from datetime import date, datetime
import time
import hashlib
import sqlite3
import shlex, subprocess
from pathlib import Path
import ssl, os

FILE_BULLETIN_BOARD = 'bulletin_board.txt'

ACTION_UPDATE = 'update'

EVENT_UPDATE_OPENWEATHERMAP = 'update_openweathermap'
EVENT_UPDATE_JIRA_INFOS = 'update_jira_infos'
EVENT_UPDATE_GITLAB = 'update_gitlab'
EVENT_UPDATE_KPI = 'update_kpi'

NAMESPACE_SERVICES = '/services'
NAMESPACE_TERMINAL_COMMAND = '/terminal-command'

SOURCE_OPENWEATHERMAP = 'openweathermap'
SOURCE_JIRA_INFOS = 'jira-infos'
SOURCE_GITLAB = 'gitlab'
SOURCE_KPI = 'kpi'

STATUS_SUCCESS = 'success'
STATUS_FAILURE = 'failure'

def get_unix_timestamp(datetime):
  return str(int(time.mktime(datetime.timetuple())))

def get_sha224(text):
  return hashlib.sha224(text.encode('utf-8')).hexdigest()

def authenticate(cursor, user, password):
  ph = get_sha224('{user}_{password}'.format(
    user = user,
    password = password
  ))
  cursor.execute('SELECT COUNT(*) FROM credentials WHERE is_activated=1 and password_hash=?', (ph, ))
  return cursor.fetchone()

def authorize(cursor, command):
  stdout = None
  params = shlex.split(command)
  executor = params.pop(0)
  results = cursor.execute('SELECT * FROM commands WHERE is_available=1 and text=?', (executor, )).fetchall()
  for row in results:
    if Path(row[3]).exists():
      evaluable = shlex.split(row[3]) + params
      process = subprocess.Popen(evaluable, stdout=subprocess.PIPE)
      stdout = process.communicate()[0]
      # if (row[4] is not None):
      #  subprocess.Popen('curl -s --noproxy 127.0.0.1,  -X POST --data "type=' + row[4] + '" http://127.0.0.1:8080/broadcast > /dev/null', shell=True)
    else:
      stdout = '[[gb;#e80000;]The specified command is unavailable.]'.encode('utf-8')
  if (stdout is not None):
    return stdout.decode('utf-8')
  else:
    return None

async def socket_emit(sid, sio, client_namespace, event, action, status):
  await sio.emit('message', {
    'status': STATUS_SUCCESS,
    'for': event
  }, namespace = client_namespace, room = sid)
  await sio.emit(event, {
    'status': status,
    'action': action
  }, namespace = '/')

sqlite_connection = sqlite3.connect('db.sqlite')
sqlite_cursor = sqlite_connection.cursor()

logging.basicConfig(filename='events.log',level=logging.DEBUG)
sio = socketio.AsyncServer()
app = web.Application()
sio.attach(app)

async def index(request):
    """Serve the client-side application."""
    with open('index.html') as f:
        return web.Response(text=f.read(), content_type='text/html')

# Broadcast namespace command
@sio.on('connect', namespace=NAMESPACE_TERMINAL_COMMAND)
def terminal_command_connect(sid, environ):
  logging.info('[{timestamp}] Client with #{sid} connected to terminal command service socket.'.format(
    timestamp = datetime.now().strftime('%d/%m/%Y %H:%M:%S'), 
    sid = sid
  ))

@sio.on('command', namespace=NAMESPACE_TERMINAL_COMMAND)
async def terminal_command_execute(sid, data):
  logging.info('[{timestamp}] Received "{data}" from #{sid}'.format(
    timestamp = datetime.now().strftime('%d/%m/%Y %H:%M:%S'),
    data = data,
    sid = sid
  ))
  # Process received data
  result = {}
  if (data['type'] == 'authentication'):
    user = data['package']['user']
    password = data['package']['password']
    is_authenticated = authenticate(sqlite_cursor, user, password)[0] == 1
    auth_token = ''
    if is_authenticated == True:
      auth_token = get_sha224('{user}_{password}_{timestamp}'.format(
        user = user,
        password = password,
        timestamp = get_unix_timestamp(datetime.now())
      ))
    result = {
      'allow': is_authenticated,
      'auth_token': auth_token
    }
  elif (data['type'] == 'execution'):
    output = authorize(sqlite_cursor, data['package']['command'])
    if (output is None):
      output = '[[gb;#e80000;]The specified command does not exist or you are not allowed to execute it.]'
    result = {
      'status': True,
      'output': str(output)
    }
  else:
    result = {
      'command_text': 'Unknown action was recorded and notified to administrator.'
    }
  logging.info('[{timestamp}] Send response to #{sid}'.format(
    timestamp = datetime.now().strftime('%d/%m/%Y %H:%M:%S'),
    sid = sid
  ))
  await sio.emit('response', {
    'status': STATUS_SUCCESS,
    'from': data['type'],
    'result': result
  }, namespace = NAMESPACE_TERMINAL_COMMAND, room = sid)

@sio.on('disconnect', namespace=NAMESPACE_TERMINAL_COMMAND)
def terminal_command_disconnect(sid):
  logging.info('[{timestamp}] Client with #{sid} disconnected from terminal command socket.'.format(
    timestamp = datetime.now().strftime('%d/%m/%Y %H:%M:%S'), 
    sid = sid
  ))

# Broadcast namespace service
@sio.on('connect', namespace=NAMESPACE_SERVICES)
def services_connect(sid, environ):
  logging.info('[{timestamp}] Client with #{sid} connected to services socket.'.format(
    timestamp = datetime.now().strftime('%d/%m/%Y %H:%M:%S'), 
    sid = sid
  ))

@sio.on('rtm_retrieve_bulletin_board', namespace='/')
async def rtm_retrieve_bulletin_board(sid, data):
  logging.info('[{timestamp}] Received bulletin board update request'.format(
    timestamp = datetime.now().strftime('%d/%m/%Y %H:%M:%S'),
    data = data,
    sid = sid
  ))
  lines = []
  with open(FILE_BULLETIN_BOARD) as f:
    lines = f.readlines()
  await sio.emit('rtm_update_bulletin_board', {
    'status': STATUS_SUCCESS,
    'content': lines,
    'formatted_timestamp': datetime.now().strftime('%d/%m/%Y %H:%M')
  }, namespace = '/')
  logging.info('[{timestamp}] Fulfilled bulletin board update request'.format(
    timestamp = datetime.now().strftime('%d/%m/%Y %H:%M:%S'),
    data = data,
    sid = sid
  ))

@sio.on('message', namespace=NAMESPACE_SERVICES)
async def services_message(sid, data):
  logging.info('[{timestamp}] Received "{data}" from #{sid}'.format(
    timestamp = datetime.now().strftime('%d/%m/%Y %H:%M:%S'),
    data = data,
    sid = sid
  ))
  if data['source'] == SOURCE_OPENWEATHERMAP:
    await socket_emit(
      sid,
      sio, 
      NAMESPACE_SERVICES,
      EVENT_UPDATE_OPENWEATHERMAP,
      ACTION_UPDATE,
      STATUS_SUCCESS
    )
  elif data['source'] == SOURCE_JIRA_INFOS:
    await socket_emit(
      sid,
      sio, 
      NAMESPACE_SERVICES,
      EVENT_UPDATE_JIRA_INFOS,
      ACTION_UPDATE,
      STATUS_SUCCESS
    )
  elif data['source'] == SOURCE_GITLAB:
    await socket_emit(
      sid,
      sio,
      NAMESPACE_SERVICES,
      EVENT_UPDATE_GITLAB,
      ACTION_UPDATE,
      STATUS_SUCCESS
    )
  elif data['source'] == SOURCE_KPI:
    await socket_emit(
      sid,
      sio,
      NAMESPACE_SERVICES,
      EVENT_UPDATE_KPI,
      ACTION_UPDATE,
      STATUS_SUCCESS
    )
  else:
    logging.info('[{timestamp}] Passing through'.format(timestamp = datetime.now().strftime('%d/%m/%Y %H:%M:%S')))
  logging.info('[{timestamp}] Emitting message to #{sid}'.format(
    timestamp = datetime.now().strftime('%d/%m/%Y %H:%M:%S'),
    sid = sid
  ))

@sio.on('disconnect', namespace=NAMESPACE_SERVICES)
def services_disconnect(sid):
  logging.info('[{timestamp}] Client with #{sid} disconnected from services socket.'.format(
    timestamp = datetime.now().strftime('%d/%m/%Y %H:%M:%S'), 
    sid = sid
  ))

# Web Application
async def broadcast(request):
  data = await request.post()
  if data['type'] == 'leave_message':
    with open(FILE_BULLETIN_BOARD) as f:
      lines = f.readlines()
    await sio.emit('rtm_update_bulletin_board', {
      'status': STATUS_SUCCESS,
      'content': lines,
      'formatted_timestamp': datetime.now().strftime('%d/%m/%Y %H:%M')
    }, namespace = '/')
  return web.json_response({ 'status': 'success' })

app.router.add_static('/static', 'static')
app.router.add_get('/', index)
app.router.add_routes([web.post('/broadcast', broadcast)])

if __name__ == '__main__':
  is_ssl = os.environ.get('SSL')
  if is_ssl is not None and is_ssl == '1':
    sslcontext = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
    sslcontext.load_cert_chain(certfile = '../certificates/cert.pem')
    web.run_app(app = app, ssl_context = sslcontext)
  else:
    web.run_app(app = app)
