import Dashboard from "views/Dashboard/Dashboard"

const dashboardRoutes = [
  {
    path: "/overview",
    name: "Overview",
    icon: "pe-7s-graph",
    component: Dashboard
  },
  { redirect: true, path: "/", to: "/overview", name: "Dashboard" }
]

export default dashboardRoutes
