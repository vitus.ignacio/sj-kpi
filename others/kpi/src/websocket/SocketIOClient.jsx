import io from 'socket.io-client';

export default class SocketIOClient {

  static _socket = null

  static initialize(bind_events) {
    let socket = SocketIOClient._socket
    if (socket === null) {
      // Currently only support HTTP
      // TODO: Allow HTTPS later
      SocketIOClient._socket = socket = io(`http://0.0.0.0:8080`);
      // Bind events
      socket.on('connect',() => { console.log('connected to socket server'); })
      socket.on('disconnect',() => { console.log('disconnected from socket server'); })
    }

    bind_events(socket);
  }
  
}
