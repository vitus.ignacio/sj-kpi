import _ from 'underscore'
import moment from 'moment'
import uuidv1 from 'uuid/v1'

import React, { Component } from 'react'
import GitlabService from '../../api/GitlabService'

import SocketIOClient from '../../websocket/SocketIOClient'

import './MergeRequest.css'

export class MergeRequest extends Component {

  static MERGE_REQUEST_STATE_COMPLETED = 'hayato.shimizu'

  static REGEX_JIRA_VERSION = /^\d{1,3}.\d{1,3}.\d{1,3}(.\d{1,3})*$/g

  constructor() {
    super()
    let _self = this

    this.state = {
      tick: setInterval(() => {
        if (_self.parent) {
          if (_self.parent.state.gitlabUpdatedOn) {
            _self.parent.setState({
              gitlabLastUpdated: moment.unix(_self.props.parent.state.gitlabUpdatedOn).fromNow()
            })
          }
        }
      }, 60000),
      mrs: []
    }
  }

  fetchGitlab() {
    let _self = this
    GitlabService.getInstance().fetchData()
      .then(res => {
        _self.parent.setState({
          gitlabLastUpdated: res.data.updated_on !== null && typeof res.data.updated_on !== 'undefined' ? moment.unix(res.data.updated_on).fromNow() : 'There was a problem retrieving timestamp.',
          gitlabUpdatedOn: res.data.updated_on,
          pendingMRs: res.data.open_merge_requests_with_frontend_label.length
        })
        _self.setState({
          mrs: res.data.open_merge_requests_with_frontend_label
        })
      })
  }

  componentDidMount() {
    let _self = this
    this.parent = this.props.parent

    SocketIOClient.initialize((socket) => {
      socket.on('update_gitlab', (data) => {
        if (data.status === 'success' &&
            data.action === 'update') {
          console.log('fetch gitlab informations started.')
          _self.fetchGitlab()
          console.log('fetch gitlab informations done.')
        }
      })
    })

    this.fetchGitlab()
  }
  
  render() {
    let mrs = []
    for (let i = 0; i < this.state.mrs.length; i++) {
      let tags = [<span key={uuidv1()} className={`merge-request__title__tag bg-color-f1ab00`}>{this.state.mrs[i].milestone}</span>]
      for (let j = 0; j < this.state.mrs[i].labels.length; j++) {
        let tag = this.state.mrs[i].labels[j],
            tag_ext_classes = []
        if (tag.toLowerCase().indexOf('ok') !== -1) {
          tag_ext_classes.push('bg-color-7e3a')
        } else {
          tag_ext_classes.push('bg-color-cd1e10')
        }
        tags.push(<span key={uuidv1()} className={`merge-request__title__tag ${tag_ext_classes.join(' ')}`}>{tag}</span>)
      }
      mrs.push(
        <tr key={uuidv1()}>
          <td className='merge-request__icon'>
            <i className={ 
              this.state.mrs[i].assignee_identifier === MergeRequest.MERGE_REQUEST_STATE_COMPLETED ? 
              'fa fa-check text-success' : 
              ( this.state.mrs[i].title.toLowerCase().indexOf('wip') !== -1 ?
              'fa fa-spinner text-danger spinable' :
              'fa fa-times text-danger scalable delay-800ms') } />
          </td>
          <td className='merge-request__title'>
            <a className={`merge-request__title__link ${this.state.mrs[i].assignee_identifier === MergeRequest.MERGE_REQUEST_STATE_COMPLETED ?
            'text-muted' : 'text-normal'}
            `} href={this.state.mrs[i].web_url} target='_blank'>{this.state.mrs[i].title}</a><br />
            {tags}
          </td>
          <td>{this.state.mrs[i].assignee}</td>
        </tr>
      )
    }
    return <tbody>{mrs}</tbody>
  }
}

export default MergeRequest
