import _ from 'underscore'
import showdown from 'showdown'
import uuidv1 from 'uuid/v1'

import SocketIOClient from '../../websocket/SocketIOClient'

import React, { Component } from "react"

import './News.css'

export class News extends Component {

  constructor() {
    super()

    this.state = {
      newsItems: []
    }
  }

  componentDidMount() {
    let _self = this

    SocketIOClient.initialize((socket) => {

      socket.on('rtm_update_bulletin_board', function(data) {
        if (data.status === 'success') {
          let items = []
          _self.setState({
            newsItems: []
          })
          console.log('fetch bulletin board informations started.')
          let rawNewsItems = _.sortBy(data.content, function(item) { return item.indexOf('pinned') }).reverse()
          _.each(rawNewsItems, (value, key, list) => {
            if (value) {
              let stuffs = value.split('~')
              if (stuffs.length > 0) {
                items.push({
                  id: stuffs[1].trim(),
                  isPinned: stuffs[0].trim() === 'pinned',
                  text: (new showdown.Converter({ openLinksInNewWindow: true })).makeHtml(stuffs[2].trim())
                })
              }
            }
          })
          _self.setState({
            newsItems: items
          })
          console.log('fetch bulletin board informations done.')
        }
      })

    })
  }

  render() {
    let newsItems = this.state.newsItems
    let newsOutputs = []
    let icon, textStyle
    for (var i = 0 ; i < newsItems.length ; i++) {
      icon = newsItems[i].isPinned ? 'fa-exclamation scalable' : 'fa-comments'
      textStyle = newsItems[i].isPinned ? 'text-danger' : 'text-normal'
      newsOutputs.push(
        <tr key={uuidv1()} className='news__item' data-id={newsItems[i].id}>
          <td className='news__icon'>
            <i className={`fa ${icon} ${textStyle}`} />
          </td>
          <td className='news__content' dangerouslySetInnerHTML={{ __html: newsItems[i].text }}></td>
          <td></td>
        </tr>
      )
    }
    return <tbody>{newsOutputs}</tbody>
  }
}

export default News
