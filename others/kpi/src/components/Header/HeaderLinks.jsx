import React, { Component } from "react"
import { Nav, NavDropdown, MenuItem } from "react-bootstrap"

class HeaderLinks extends Component {

  constructor() {
    super()
    this.state = {
      notifications: [],
      hasNotifications: false
    }
  }

  retrieveNotifications() {
    // TODO: Get notification data
    this.setState({
      hasNotifications: this.state.notifications.length > 0
    })
  }

  componentDidMount() {
    this.retrieveNotifications()
  }

  render() {
    const notification = (
      <div>
        <i className="fa fa-globe" />
        <b className="caret" />
        <span className="notification">{this.state.notifications.length}</span>
        <p className="hidden-lg hidden-md">Notification</p>
      </div>
    )
    return (
      <div>
        <Nav>
          <NavDropdown
            eventKey={1}
            title={this.state.hasNotifications ? notification : ''}
            noCaret
            id="basic-nav-dropdown"
          >
            <MenuItem eventKey={1.1}>No new notification</MenuItem>
          </NavDropdown>
        </Nav>
      </div>
    )
  }
}

export default HeaderLinks
