import axios from 'axios'
import uuidv1 from 'uuid/v1'

export default class GitlabService {

  SERVICE_API_URL = `api/v1/gitlab.json?_=${uuidv1()}`

  static _instance = null

  static getInstance() {
    if (GitlabService._instance == null) {
      GitlabService._instance = new GitlabService()
    }
    return GitlabService._instance
  }

  fetchData() {
    return axios.get(this.SERVICE_API_URL)
  }

}
