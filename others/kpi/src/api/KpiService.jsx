import axios from 'axios'
import uuidv1 from 'uuid/v1'

export default class KpiService {

  SERVICE_KPI_API_URL = `api/v1/productivity_chart_data.json?_=${uuidv1()}`
  SERVICE_JIRA_INFOS_API_URL = `api/v1/jira_infos.json?_=${uuidv1()}`

  static _instance = null

  static getInstance() {
    if (KpiService._instance == null) {
      KpiService._instance = new KpiService()
    }
    return KpiService._instance
  }

  fetchKpiData() {
    return axios.get(this.SERVICE_KPI_API_URL)
  }

  fetchJiraInfosData() {
    return axios.get(this.SERVICE_JIRA_INFOS_API_URL)
  }

}
