import _ from 'underscore'
import moment from 'moment'
import randomColor from 'randomcolor'

import React, { Component } from 'react'
import { Doughnut, Line } from 'react-chartjs-2'
import { Grid, Row, Col } from 'react-bootstrap'

import SocketIOClient from '../../websocket/SocketIOClient'

import KpiService from '../../api/KpiService'

import { Card } from 'components/Card/Card.jsx'
import { StatsCard } from 'components/StatsCard/StatsCard.jsx'
import { News } from 'components/News/News.jsx'
import { MergeRequest } from 'components/MergeRequest/MergeRequest.jsx'

import './Dashboard.css'

class Dashboard extends Component {

  static CURRENT_SPRINT_RELEASE = '_._._'

  constructor() {
    super()
    let _self = this

    this.kpiService = KpiService.getInstance()

    this.state = {
      tick: setInterval(() => {
        if (_self.state.kpiUpdatedOn) {
          _self.setState( {
            kpiLastUpdated: moment.unix(_self.state.kpiUpdatedOn).fromNow()
          })
        }
        if (_self.state.jiraUpdatedOn) {
          moment().from(moment(_self.state.jiraUpdatedOn))
        }
      }, 60000),

      currentSprint: Dashboard.CURRENT_SPRINT_RELEASE,
      currentRelease: Dashboard.CURRENT_SPRINT_RELEASE,
      pendingMRs: 0,
      pendingTasks: 0,

      kpiData: [],
      kpiLabels: [],
      kpiLastUpdated: null,
      kpiUpdatedOn: null,

      jiraData: {},
      jiraLastUpdated: null,
      jiraUpdatedOn: null
    }
  }

  fetchJiraInfos() {
    let _self = this

    this.kpiService.fetchJiraInfosData()
      .then(res => {
        let jiraData = {
          labels: ['Done', 'In-Progress', 'QC/QA'],
          datasets: [
            {
              data: [
                res.data.done_tasks,
                res.data.doing_tasks,
                res.data.qcqa_tasks
              ],
              backgroundColor: [
                  '#007e3a',
                  '#f1ab00',
                  '#cd1e10',
              ]
            }
          ] 
        }

        _self.setState({
          currentRelease: res.data.current_release,
          currentSprint: res.data.current_sprint,
          pendingTasks: res.data.doing_tasks,
          jiraData: jiraData,
          jiraLastUpdated: 'There was a problem retrieving update timestamp.',
          jiraUpdatedOn: null
        })
      })
  }

  fetchKpi() {
    let _self = this

    this.kpiService.fetchKpiData()
      .then(res => {
        let max_productivity_data_count = 0,
            last_updated = 0,
            kpiData = [],
            labels = []

        _.each(res.data, (value, key, list) => {
          let color = randomColor({
            luminosity: 'dark',
            format: 'rgb'
          })
          if (value.productivity_data.length > max_productivity_data_count) {
            max_productivity_data_count = value.productivity_data.length
          }
          if (value.last_updated > last_updated) {
            last_updated = value.last_updated
          }
          kpiData.push({
            label: value.friendly_id,
            backgroundColor: 'transparent',
            borderColor: color,
            data: value.productivity_data
          })
        })

        for (let i = 0; i < max_productivity_data_count; i++) {
          if (i === 9) {
            labels.push('Current')
          } else {
            labels.push(i)
          }
        }

        _self.setState({
          kpiLabels: labels,
          kpiData: kpiData,
          kpiLastUpdated: moment.unix(last_updated).fromNow(),
          kpiUpdatedOn: last_updated
        })
      })

    SocketIOClient._socket.emit('rtm_retrieve_bulletin_board', null)
  }

  componentDidMount() {
    let _self = this

    SocketIOClient.initialize((socket) => {
      socket.on('update_jira_infos', (data) => {
        if (data.status === 'success' &&
            data.action === 'update') {
          console.log('fetch JIRA informations started.')
          _self.fetchJiraInfos()
          console.log('fetch JIRA informations done.')
        }
      })
      socket.on('update_kpi', (data) => {
        if (data.status === 'success' &&
            data.action === 'update') {
          console.log('fetch kpi informations started.')
          _self.fetchKpi()
          console.log('fetch kpi informations done.')
        }
      })
    })

    this.fetchJiraInfos()
    this.fetchKpi()
  }

  render() {
    return (
      <div className='content'>
        <Grid fluid>
          <Row>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i />}
                statsText='Current Sprint'
                statsValue={this.state.currentSprint}
                statsIcon={<i className='fa fa-refresh' />}
                statsIconText={null}
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i />}
                statsText='Current Release'
                statsValue={this.state.currentRelease}
                statsIcon={<i className='fa fa-refresh' />}
                statsIconText='Updated now'
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className='fa fa-gitlab text-danger' />}
                statsText='Pending MRs'
                statsValue={this.state.pendingMRs}
                statsIcon={<i className='fa fa-clock-o' />}
                statsIconText='In the last hour'
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className='fa fa-tasks text-info' />}
                statsText='Pending Tasks'
                statsValue={this.state.pendingTasks}
                statsIcon={<i className='fa fa-refresh' />}
                statsIconText='Updated now'
              />
            </Col>
          </Row>
          <Row>
            <Col md={8}>
              <Card
                statsIcon='fa fa-history'
                id='chartHours'
                title='Productivity'
                category='24 Hours performance'
                stats={`Updated ${this.state.kpiLastUpdated}`}
                content={
                  <div className='ct-chart'>
                    <Line data={{
                      labels: this.state.kpiLabels,
                      datasets: this.state.kpiData
                    }} options={{
                      responsive: true,
                      maintainAspectRatio: false,
                      elements: { point: { radius: 0 } } 
                    }} />
                  </div>
                }
              />
            </Col>
            <Col md={4}>
              <Card
                statsIcon='fa fa-clock-o'
                title='JIRA Progress Report'
                category='In-Progress/Done/QCQA'
                stats={this.state.jiraLastUpdated}
                content={
                  <div
                    id='chartPreferences'
                    className='ct-chart ct-perfect-fourth'
                  >
                    <Doughnut data={this.state.jiraData} options={{
                      responsive: true,
                      maintainAspectRatio: false
                    }} />
                  </div>
                }
              />
            </Col>
          </Row>

          <Row>
            <Col md={6}>
              <Card
                title='Gitlab Merge Requests'
                category='Show latest statuses for all MRs'
                stats={`Updated ${this.state.gitlabLastUpdated}`}
                statsIcon='fa fa-history'
                content={
                  <div className='table-full-width'>
                    <table className='table'>
                      <thead>
                        <tr>
                          <th width='5%'></th>
                          <th>Title</th>
                          <th>Assignee</th>
                        </tr>
                      </thead>
                      <MergeRequest parent={this} />
                    </table>
                  </div>
                }
              />
            </Col>
            <Col md={6}>
              <Card
                title='Bulletin Board'
                category='Where you find what you need'
                stats={null}
                statsIcon='fa fa-history'
                content={
                  <div className='table-full-width'>
                    <table className='table card__news-list'>
                      <News />
                    </table>
                  </div>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    )
  }
}

export default Dashboard
