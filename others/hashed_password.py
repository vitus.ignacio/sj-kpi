import hashlib, sys

def get_sha224(text):
  return hashlib.sha224(text.encode('utf-8')).hexdigest()

if (len(sys.argv) is 3):
  user = sys.argv[1]
  password = sys.argv[2]
  sha224_p = get_sha224('{user}_{password}'.format(
    user = user,
    password = password
  ))
  print(sha224_p)
else:
  print('[ERROR] Too many or too few command-line arguments.')
