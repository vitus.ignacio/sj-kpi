import * as $ from 'jquery'
import Promise from 'promise-polyfill'
import io from 'socket.io-client'
import { initNotifications, notify } from 'browser-notification';

import './dashboard'
import './charts'
import './todos'

export default (function () {
  // When document is ready, establish connection to socket server
  const HTTP_SOCKETIO_URL = 'http://host_address:8080'
  const HTTPS_SOCKETIO_URL = 'https://host_address:8443'
  var base_socketio_url = ''

  if (window.location.protocol != 'https:') {
    base_socketio_url = HTTP_SOCKETIO_URL
  } else {
    base_socketio_url = HTTPS_SOCKETIO_URL
  }

  window.socket = io(`${base_socketio_url}`)
  window.services_socket = io(`${base_socketio_url}/services`)
  window.socket.on('connect', function() {
    console.log('connected to socket io server.')
  })
  window.socket.on('disconnect', function() {
    console.log('disconnected from socket io server.')
  })
  window.services_socket.on('connect', function() {
    console.log('connected to services socket io server.')
  })
  window.services_socket.on('disconnect', function() {
    console.log('disconnected from services socket io server.')
  })

  // Browser notification
  initNotifications()
  window.notify = notify
})();
