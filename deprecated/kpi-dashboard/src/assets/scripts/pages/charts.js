import * as $ from 'jquery';
import * as moment from 'moment';
import * as _ from 'underscore';
import Chart from 'chart.js';

function fetchProductivityData(data_url, productivity_line_chart) {
  // Get productivity data
  $.get(data_url, function (response) {
    if (response) {
      var max_labels_count = 0
      var last_updated = null
      _.each(response, function (kpi_member, index, list) {
        if (last_updated == null) {
          last_updated = kpi_member.last_updated
        }
        if (kpi_member.productivity_data.length > max_labels_count) {
          max_labels_count = kpi_member.productivity_data.length
        }
        var kpi_chart_member = _.find(productivity_linechart.data.datasets, function (kpi_chart_member) {
          return kpi_chart_member.label == kpi_member.friendly_id
        })
        kpi_chart_member.data = kpi_member.productivity_data
      })
      productivity_linechart.data.labels = _.range(max_labels_count - 1)
      if (last_updated != null) {
        productivity_linechart.data.labels.push(moment.unix(last_updated).format('DD/MM/YYYY'))
      } else {
        productivity_linechart.data.labels.push(moment().format('DD/MM/YYYY'))
      }
      
      productivity_linechart.update()
    }
  })
}

export default (function() {

  $(function () {

    if ($('.js-charts-module').length > 0) {
      const PRODUCTIVITY_DATA_URL = 'assets/static/data/productivity_chart_data.json?_=' + _.random(0, 9999999)
      
      var productivity_linechart_element = $('#js-productivity-linechart').get(0)

      if (productivity_linechart_element)
      {
        window.socket.on('update_kpi', function(data) {
          if (data.status == 'success' &&
              data.action == 'update') {
            console.log('fetch productivity informations started.')
            fetchProductivityData(PRODUCTIVITY_DATA_URL, window.productivity_linechart)
            console.log('fetch productivity informations done.')
          }
        })

        // Prepare productivity line chart
        var productivity_linechart = new Chart(productivity_linechart_element, {
            type: 'line',
            data: 
            {
              labels: [],
              datasets: [
                {
                  label: 'tu.truong',
                  backgroundColor: 'transparent',
                  borderColor: 'rgb(255, 99, 132)',
                  data: []
                },
                {
                  label: 'luohao',
                  backgroundColor: 'transparent',
                  borderColor: 'rgb(120, 40, 200)',
                  data: []
                },
                {
                  label: 'tram.nguyen',
                  backgroundColor: 'transparent',
                  borderColor: 'rgb(40, 140, 20)',
                  data: []
                },
                {
                  label: 'hung.nguyen',
                  backgroundColor: 'transparent',
                  borderColor: 'rgb(65, 40, 128)',
                  data: []
                }
              ]
            },
            options: {
              maintainAspectRatio: false,
              elements: { point: { radius: 0 } } 
            }
        })

        window.productivity_linechart = productivity_linechart

        fetchProductivityData(PRODUCTIVITY_DATA_URL, productivity_linechart)
      }
    }

  })

})();
