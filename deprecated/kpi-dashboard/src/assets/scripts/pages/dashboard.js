import * as $ from 'jquery'
import * as moment from 'moment'
import * as _ from 'underscore'
import * as showdown from 'showdown'
import * as removeMd from 'remove-markdown'

const OPENWEATHERMAP_DATA_URL = 'assets/static/data/openweathermap.json?_=' + _.random(1, 999999)
const MONTHLY_RANKING_DATA_URL = 'assets/static/data/monthly_ranking.json?_=' + _.random(1, 999999)
const JIRA_INFOS_DATA_URL = 'assets/static/data/jira_infos.json?_=' + _.random(1, 999999)
const GITLAB_DATA_URL = 'assets/static/data/gitlab.json?_=' + _.random(1, 999999)
const COMPLETABLE_USER = 'hayato.shimizu'

function fetchGitlab() {
  return new Promise(function(resolve, reject) {
    $.get(GITLAB_DATA_URL, function(response) {
      if (response) {
        let open_gitlab_issues_elements = $('.js-open-gitlab-issues')
        let last_updated_on_gitlab_elements = $('.js-last-updated-on-gitlab')

        let last_updated_on = null

        open_gitlab_issues_elements.html('')

        let badge_tag_template = `
          <span class="badge badge-pill fl-l badge-info dashboard__rank--first lh-0 p-10 dashboard__gitlab__tag {1}">{0}</span>
        `

        let open_gitlab_issues_template = `
        <tr class="{0}">
          <td scope="row"><img src="{1}" width="24" height="24" /></td>
          <td>
            <span class="{7}">{6}</span>
            <a class="dashboard__a-no-decoration {5}" href="{4}" target="_blank">{2}</a>
            <br />
            <span class="badge badge-pill fl-l badge-info dashboard__rank--second lh-0 p-10 dashboard__gitlab__tag">{8}</span>
            {9}
          </td>
          <td>{3}</td>
        </tr>
        `

        _.each(_.sortBy(response.open_merge_requests_with_frontend_label,
          function(value) {
            return value.work_in_progress
          }).reverse(),
        function(value, key, list) {
          var isCompleted = !value.work_in_progress && value.assignee_identifier == COMPLETABLE_USER
          var tags = ''
          _.each(value.labels, function(tag, tag_key, tags_list) {
            tags += badge_tag_template
              .replace('{0}', tag)
              .replace('{1}', tag.toLowerCase().indexOf('ok') != -1 ? 'dashboard__rank--third' : '' )
          })
          open_gitlab_issues_elements.append(
            open_gitlab_issues_template
              .replace('{0}', value.work_in_progress ? 'dashboard__label--important' : 'dashboard__label--normal')
              .replace('{1}', value.assignee_avatar)
              .replace('{2}', value.title)
              .replace('{3}', value.assignee)
              .replace('{4}', value.web_url)
              .replace('{5}', value.work_in_progress ? 'dashboard__label--important' : 'dashboard__label--normal')
              .replace('{6}', isCompleted ? '✔' : '✘')
              .replace('{7}', isCompleted ? 'dashboard__label--completed' : 'dashboard__label--important')
              .replace('{8}', value.milestone)
              .replace('{9}', tags)
          )
        })
        last_updated_on = response.updated_on
        last_updated_on_gitlab_elements.text(
          last_updated_on != null ?
            moment.unix(last_updated_on).format('DD/MM/YYYY hh:mm A') :
            '--/--/---- --:-- --'
        )
      }
    })
  })
}

function fetchJiraInfos() {
  return new Promise(function(resolve, reject) {
    $.get(JIRA_INFOS_DATA_URL, function(response) {
      if (response) {
        let current_sprint_elements = $('.js-current-sprint')
        let current_release_elements = $('.js-current-release')
        let jira_tasks_elements = $('.js-jira-tasks')
        let jira_frontend_tasks_elements = $('.js-jira-frontend-tasks')
        let jira_sjdev_tasks_elements = $('.js-jira-sjdev-tasks')
        let jira_mt_tasks_elements = $('.js-jira-mt-tasks')
        let jira_done_tasks_percentage_elements = $('.js-jira-done-tasks')
        let jira_doing_tasks_percentage_elements = $('.js-jira-doing-tasks')
        let jira_qcqa_tasks_percentage_elements = $('.js-jira-qcqa-tasks')

        current_sprint_elements.text(response.current_sprint)
        current_release_elements.text(response.current_release)
        jira_tasks_elements.text(response.all_tasks)
        jira_frontend_tasks_elements.text(response.frontend_tasks)
        jira_sjdev_tasks_elements.text(response.sjdev_tasks)
        jira_mt_tasks_elements.text(response.mt_tasks)

        let data_jira_done_tasks = jira_done_tasks_percentage_elements.data('easyPieChart')
        let data_jira_doing_tasks = jira_doing_tasks_percentage_elements.data('easyPieChart')
        let data_jira_qcqa_tasks = jira_qcqa_tasks_percentage_elements.data('easyPieChart')
        if (data_jira_done_tasks) {
          data_jira_done_tasks.update(response.done_tasks)
        }
        if (data_jira_doing_tasks) {
          data_jira_doing_tasks.update(response.doing_tasks)
        }
        if (data_jira_qcqa_tasks) {
          data_jira_qcqa_tasks.update(response.qcqa_tasks)
        }
      }
      resolve()
    })
  })
}

function fetchWeatherInfos() {
  return new Promise(function (resolve, reject) {
    $.get(OPENWEATHERMAP_DATA_URL, function(response) {
      if (response) {
        let current_temperature_elements = $('.js-current-temperature')
        let current_weather_condition_elements = $('.js-weather-condition')
        let current_weather_condition_icon_element = $('#js-weather-condition-icon')
        let wind_speed_elements = $('.js-wind-speed')
        let sunrise_time_elements = $('.js-sunrise-time')
        let sunset_time_elements = $('.js-sunset-time')
        let pressure_elements = $('.js-pressure')
        let last_updated_on_openweathermap_elements = $('.js-last-updated-on-openweathermap')
        let now_date_elements = $('.js-now-date')

        let current_temperature = Math.round(parseFloat(response.main.temp) - 273.15 , 0)
        let current_weather_condition = response.weather[0].main.trim()
        let current_weather_condition_icon = response.weather[0].icon
        let wind_speed = response.wind.speed + ' m/s'
        let sunrise_time = moment.unix(parseInt(response.sys.sunrise)).format('DD/MM/YYYY hh:mm A')
        let sunset_time = moment.unix(parseInt(response.sys.sunset)).format('DD/MM/YYYY hh:mm A')
        let pressure = response.main.pressure + ' hPa'
        let last_updated_on = moment.unix(parseInt(response.updated)).format('DD/MM/YYYY hh:mm A')
        let now_date = moment().format('dddd DD/MM/YYYY')

        current_temperature_elements.text(current_temperature)
        current_weather_condition_elements.text(current_weather_condition)
        wind_speed_elements.text(wind_speed)
        sunrise_time_elements.text(sunrise_time)
        sunset_time_elements.text(sunset_time)
        pressure_elements.text(pressure)
        last_updated_on_openweathermap_elements.text(last_updated_on)
        now_date_elements.text(now_date)
        current_weather_condition_icon_element.attr('class', '')
        switch (current_weather_condition_icon)
        {
          case '04d':
            current_weather_condition_icon_element.addClass('cloudy')
            Skycons.DrawSkycons()
            break
          case '03n':
            current_weather_condition_icon_element.addClass('cloudy')
            Skycons.DrawSkycons()
            break
          case '03d':
            current_weather_condition_icon_element.addClass('cloudy')
            Skycons.DrawSkycons()
            break
          case '11d':
            current_weather_condition_icon_element.addClass('rain')
            Skycons.DrawSkycons()
            break;
          default:
            break
        }
      }
      resolve()
    })  
  })
}

function fetchMonthlyRanking() {
  return new Promise(function(resolve, reject) {
    $.get(MONTHLY_RANKING_DATA_URL, function(response) {
      if (response) {
        let monthly_ranking_elements = $('.js-monthly-ranking')

        let monthly_ranking_template = `
        <li class="list-group-item bdw-0 dashboard__pL-0">
          <div class="peers ai-c">
            <label for="inputCall1" class=" peers peer-greed js-sb ai-c">
              <span class="peer peer-greed">{0}</span>
              <span class="peer">
                <span class="badge badge-pill fl-r badge-info {2} lh-0 p-10">{1}</span>
              </span>
            </label>
          </div>
        </li>
        `
        monthly_ranking_elements.html('')
        _.each(_.sortBy(response, 'rank').reverse(), function(value, key, list) {
          var actual_rank = '?'
          var actual_rank_class = ''
          switch (key) {
            case 0:
              actual_rank = 'gold'
              actual_rank_class = 'dashboard__rank--first'
              break
            case 1:
              actual_rank = 'silver'
              actual_rank_class = 'dashboard__rank--second'
              break
            case 2:
              actual_rank = 'bronze'
              actual_rank_class = 'dashboard__rank--third'
              break
            default: 
              actual_rank = 'complimentary prize'
              break
          }
          monthly_ranking_elements.append(
            monthly_ranking_template
              .replace('{0}', value.name)
              .replace('{1}', actual_rank)
              .replace('{2}', actual_rank_class)
          )
        })
      }
      resolve()
    })
  })
}

function fetchBulletinBoard() {
  window.services_socket.emit('rtm_retrieve_bulletin_board', null)
}

export default (function() {

  $(function() {

    if ($('.js-dashboard-module').length > 0) {

      window.socket.on('update_jira_infos', function(data) {
        if (data.status == 'success' &&
            data.action == 'update') {
          console.log('fetch JIRA informations started.')
          fetchJiraInfos()
          console.log('fetch JIRA informations done.')
        }
      })
      window.socket.on('update_openweathermap', function(data) {
        if (data.status == 'success' &&
            data.action == 'update') {
          console.log('fetch weather informations started.')
          fetchWeatherInfos()
          console.log('fetch weather informations done.')
        }
      })
      window.socket.on('update_gitlab', function(data) {
        if (data.status == 'success' &&
            data.action == 'update') {
          console.log('fetch gitlab informations started.')
          fetchGitlab()
          console.log('fetch gitlab informations done.')
        }
      })
      window.services_socket.on('rtm_update_bulletin_board', function(data) {
        var item_template = `<li data-id="{1}" class="list-group-item bdw-0 pX-0 pY-0">{0}</li>`
        if (data.status == 'success') {
          console.log('fetch bulletin board informations started.')
          var news_items = '',
              news_collections_latest = _.sortBy(data.content, function(item) { return item.indexOf('pin.png') }).reverse()
          _.each(news_collections_latest, function(news, key, list) {
            var news_fragments = news.split('~')
            if (news_fragments.length > 0) {
              var news_id = news.split('~')[0],
                  news_content = news.split('~')[1]
              news_items += item_template
                            .replace('{0}', (new showdown.Converter({ openLinksInNewWindow: true })).makeHtml(news_content))
                            .replace('{1}', news_id)
            }
          })
          $('.js-bulletin-board-content').html(news_items)
          // window.notify('FE Bulletin Board', {
          //   body: removeMd(news_collections_latest[0].split('~')[1].replace(/\[[0-9]{2}\/[0-9]{2}\/[0-9]{4} [0-9]{2}:[0-9]{2}\]/i, ''))
          // })
          $('.js-last-updated-on-bulletin-board').html(data.formatted_timestamp)
          console.log('fetch bulletin board informations done.')
        }
      })

      // Get informations
      fetchWeatherInfos()
      fetchMonthlyRanking() 
      fetchJiraInfos()
      fetchGitlab()
      fetchBulletinBoard()
    }
  })

})();
