import * as $ from 'jquery';
import * as moment from 'moment';
import * as _ from 'underscore';

export default (function() {
  $(function () {
    if ($('.js-todos-module').length > 0) {
      if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $('.js-external-workast').show()
        $('.js-inline-workast').hide()
      } else {
        $('.js-external-workast').hide()
        $('.js-inline-workast').show()
      }
    }
  })
})();
