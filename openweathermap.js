var _ = require('underscore'),
    axios = require('axios'),
    fs = require('fs'),
    moment = require('moment'),
    schedule = require('node-schedule'),
    yaml = require('node-yaml'),
    logger = require('simple-node-logger').createSimpleFileLogger('logs/openweathermap-events.log'),
    utils = require('./utils')

var send_slack_webhook = process.env.SLACK_WEBHOOK || false,
    scheduled_run = process.env.SCHEDULED_RUN || false,
    data_to_file = '{}'

;(function() {

  logger.info('Read configuration file.')
  yaml.read('config.yml',
    {
      encoding: 'utf-8',
      schema: yaml.schema.defaultSafe,
    }, function(err, config) {

      var API_KEY = config.openweathermap.api_key,
          DEFAULT_CITY_LOCATION = 'ho%20chi%20minh%20city',
          DATA_URL = 'http://api.openweathermap.org/data/2.5/weather?q=' + DEFAULT_CITY_LOCATION + '&APPID=' + API_KEY + '&_=' + _.random(1, 99999999).toString()
      
      var original_file_path = `${config.paths.default_orig}/${config.output_files[4]}`,
          destination_file_path = `${config.paths.default_dest}/${config.output_files[4]}`

      logger.info(`Service started.`)

      var job = function() {
        logger.info(`Get weather informations from openweathermap.`)
        axios.get(DATA_URL)
            .then(function(response) {
              if (response) {
                if (response.data) {
                  response.data.updated = moment().unix()
                  data_to_file = response.data
                }
                if (send_slack_webhook == false) {
                  logger.info(`Write data to file ${original_file_path}`)
                  fs.writeFileSync(original_file_path,JSON.stringify(data_to_file))

                  logger.info(`Copy data file to destination folder`)
                  if (fs.existsSync(original_file_path))
                  {
                    fs.copyFileSync(original_file_path, destination_file_path)
                    utils.log_changed_file(config, 4)
                  }
                }
              }

              logger.info(`Complete retrieving weather informations from openweathermap.`)
            })
      }

      if (scheduled_run == true) {
        logger.info('Scheduled job activate.')
        schedule.scheduleJob('*/15 * * * *', job)
      } else {
        logger.info('Single-run job activate.')
        job()
      }

    })

})()

var done_yet = false
process.on('beforeExit', function() {
  if (!done_yet) {
    logger.info(`Service stopped.`)
    done_yet = true
  }
})
