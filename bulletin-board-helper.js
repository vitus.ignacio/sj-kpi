var _ = require('underscore'),
    fs = require('fs'),
    yaml = require('node-yaml'),
    moment = require('moment'),
    logger = require('simple-node-logger').createSimpleFileLogger('logs/notify-helper-events.log'),
    CONSTANTS = require('./constants'),
    notifier = require('node-notifier'),
    removeMd = require('remove-markdown')

;(function() {

  logger.info('Read configuration file.')
  yaml.read('config.yml',
    {
      encoding: 'utf-8',
      schema: yaml.schema.defaultSafe,
    }, function(err, config) {

      logger.info('Service started.')
      var socket = require('socket.io-client')(`http://${config.socketio.server_ip}:8080/services`)
      socket.on('connect', function() { })
      socket.io.on('connect_error', function() {
        logger.info(`Unable to connect to ${config.socketio.server_ip}`)
      })
      socket.on('rtm_update_bulletin_board', function(response) {
        logger.info(`Show notification.`)
        var status = response.status,
            content = response.content
        if (response.status == 'success') {
            notifier.notify({
            title: 'Bulletin Board',
            message: removeMd(content[content.length - 1].split('~')[1].replace(/\[[0-9]{2}\/[0-9]{2}\/[0-9]{4} [0-9]{2}:[0-9]{2}\]/i, ''))
          })
        }
      })
      socket.on('disconnect', function() { })

    }
  )
})()

var done_yet = false
process.on('beforeExit', function() {
  if (!done_yet) {
    logger.info(`Service stopped.`)
    done_yet = true
  }
})
