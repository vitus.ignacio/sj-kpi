const cron = require('node-schedule'),
      shell = require('shelljs'),
      logger = require('simple-node-logger').createSimpleFileLogger('logs/cron-helper-events.log')

logger.info(`Service started.`)

cron.scheduleJob('* * * * *', function(){
  logger.info(`Sub-service socketio-helper.js started.`)
  shell.exec('node ./socketio-helper.js')
  logger.info(`Sub-service socketio-helper.js stopped.`)
})

cron.scheduleJob('*/15 * * * 1,2,3,4,5', function(){
  logger.info(`Sub-service gitlab.js started.`)
  shell.exec('ENV=production SLACK_WEBHOOK=0 SCHEDULED_RUN=0 node ./gitlab.js')
  logger.info(`Sub-service gitlab.js stopped.`)
})

cron.scheduleJob('*/10 * * * 1,2,3,4,5', function(){
  logger.info(`Sub-service jira.js started.`)
  shell.exec('ENV=production SLACK_WEBHOOK=0 SCHEDULED_RUN=0 node ./jira.js')
  logger.info(`Sub-service jira.js stopped.`)
})

cron.scheduleJob('*/20 * * * 1,2,3,4,5', function(){
  logger.info(`Sub-service kpi.js started.`)
  shell.exec('ENV=production SLACK_WEBHOOK=0 SCHEDULED_RUN=0 node ./kpi.js')
  logger.info(`Sub-service kpi.js stopped.`)
})

var done_yet = false
process.on('beforeExit', function() {
  if (!done_yet) {
    logger.info(`Service stopped.`)
    done_yet = true
  }
})
