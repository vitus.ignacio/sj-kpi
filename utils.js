var fs = require('fs')

module.exports = {
  log_changed_file: function(config, index) {
    fs.open(config.socketio.changed_files, 'a+', function (err, fd) {
      if (err) return
      fs.appendFileSync(fd, `${config.output_files[index]}${require('os').EOL}`)
    })
  }
}
